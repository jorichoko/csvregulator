# CSVRegulator

A CSV to CSV Translator.

For example, when you have a program that use CSV as input, that program require specific format of CSV... quotation, escape, line break etc. However, ALL of CSV does not fit that format. So, we need CSVRegulator.

## Input/Output Format Variation

### Allow Missing Column Names

When input is CSV file with header line, header require column names for all column. If Allow Missing Column Names option is true, CSVRegulator accept missing column names.

### Comment Marker

If you set Comment Marker, lines between Comment Marker are skipped.

### Delimiter

Field delimiter. Default is comma, but you can specify (horizontal tab, for example).

### Escape Character

When you want to delimiter character or quote character, control character in quoted field, you have to use escape sequence. Escape character indicates beginning of escape sequence.

If you don't want it, you leave escape character null.

### Header

First record of CSV file, may be header record that contains column names.

By another way, you can specify header definition as a configuration.

### Ignore Empty Lines

CSV file may have empty lines. You can choose empty line as no values or ignore.

### Ignore Header Case

You can choose column names in header case-sensitive or not.

### Ignore Surrounding Spaces

You can choose spaces Surrounding field value effective or not.

### Null String

If you want distinguish between zero-length string and null, you set Null String parameter and transform Null String to null.

### Quote Character

You can choose quote character (may be null).

### Quote Mode (Quote Policy)

You can choose quote mode.

* ALL: all fields are quoted.
* MINIMAL: minimal fields are quoted.(ex. fields has delimiter as a value)
* NON_NUMERIC: numeric fields aren't quoted. other fields are quoted.
* NONE: never quotes fields.

### Record Separator

You can choose record separator (default is line break).
(This option is OUTPUT only.)

### Skip Header Record

You can choose evaluate header or not.

### Trailing delimiter

You can choose exist trailing delimiter or not.

### Trim

You can choose remove leading and trailing spaces or not.
